<?php

/*
 * Fusionary PHP Utility Belt
 *
 * A collection of methods for general use.
 */

namespace Fusionary\UtilityBelt;

class UtilityBelt
{
    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get public properties
     */
    public static function getPublicProperties($obj)
    {
        $reflect = new \ReflectionObject($obj);
        $props = array();

        foreach ($reflect->getProperties(\ReflectionProperty::IS_PUBLIC) as $obj) {
            $props[] = $obj->name;
        }
        return $props;
    }

    /**
     * Remove www from a URL string
     */
    public static function removeWww($url) {
        $url = preg_replace('#^(http(s)?://)?w{$3}\.(\w+\.\w+)#', '$1$3', $url);
        return $url;
    }

    /**
     * Version of array_merge_recursive without overwriting numeric keys
     *
     * @param  array $array1 Initial array to merge.
     * @param  array ...     Variable list of arrays to recursively merge.
     *
     * @link   http://www.php.net/manual/en/function.array-merge-recursive.php#106985
     * @author Martyniuk Vasyl <martyniuk.vasyl@gmail.com>
     */
    public static function mergeRecursively()
    {
        $arrays = func_get_args();
        $base = array_shift($arrays);

        foreach($arrays as $array) {
            reset($base);
            while(list($key, $value) = @each($array)) {
                if(is_array($value) && @is_array($base[$key])) {
                    $base[$key] = self::mergeRecursively($base[$key], $value);
                }
                else {
                    $base[$key] = $value;
                }
            }
        }

        return $base;
    }

    /**
     * Convert under_score type array's keys to camelCase type array's keys
     * @param   array   $array          array to convert
     * @param   array   $arrayHolder    parent array holder for recursive array
     * @return  array   camelCase array
     */
    public static function camelCaseKeys($array, $arrayHolder = array()) {
        $camelCaseArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = @explode('_', $key);
            array_walk($newKey, create_function('&$v', '$v = ucwords($v);'));
            $newKey = @implode('', $newKey);
            $newKey{0} = strtolower($newKey{0});
            if (!is_array($val)) {
                $camelCaseArray[$newKey] = $val;
            } else {
                $camelCaseArray[$newKey] = self::camelCaseKeys($val, $camelCaseArray[$newKey]);
            }
        }
        return $camelCaseArray;
    }

    /**
     * Convert camelCase type array's keys to under_score+lowercase type array's keys
     * @param   array   $array          array to convert
     * @param   array   $arrayHolder    parent array holder for recursive array
     * @return  array   under_score array
     */
    public static function underscoreKeys($array, $arrayHolder = array()) {
        $underscoreArray = !empty($arrayHolder) ? $arrayHolder : array();
        foreach ($array as $key => $val) {
            $newKey = preg_replace('/[A-Z]/', '_$0', $key);
            $newKey = strtolower($newKey);
            $newKey = ltrim($newKey, '_');
            if (!is_array($val)) {
                $underscoreArray[$newKey] = $val;
            } else {
                $underscoreArray[$newKey] = self::underscoreKeys($val, $underscoreArray[$newKey]);
            }
        }
        return $underscoreArray;
    }

    /**
     * Convert camelCase type array's values to under_score+lowercase type array's values
     * @param   mixed   $mixed          array|string to convert
     * @param   array   $arrayHolder    parent array holder for recursive array
     * @return  mixed   under_score array|string
     */
    public static function underscoreValues($mixed, $arrayHolder = array()) {
        $underscoreArray = !empty($arrayHolder) ? $arrayHolder : array();
        if (!is_array($mixed)) {
            $newVal = preg_replace('/[A-Z]/', '_$0', $mixed);
            $newVal = strtolower($newVal);
            $newVal = ltrim($newVal, '_');
            return $newVal;
        } else {
            foreach ($mixed as $key => $val) {
                $underscoreArray[$key] = self::underscoreValues($val, $underscoreArray[$key]);
            }
            return $underscoreArray;
        }
    }

    /**
     * Get IP address
     * @return  string
     */
    Public static function getIpAddress()
    {
        $ip = '';
        $envs = array(
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'REMOTE_ADDR',
        );

        foreach ($envs as $key) {
            if ( isset($_SERVER[$key]) ) {
                $ip = $_SERVER[ $key ];
                if ($ip) {
                    break;
                }
            }
        }

        $ips = preg_split('/,\s*/', $ip);
        $ip = trim($ips[0]);
        if (!$ip) {
            $ip = '0.0.0.0';
        }
        return $ip;
    }
}
